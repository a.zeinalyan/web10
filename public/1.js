$(document).ready(function() {
  $("#slider").slick({
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows: true,
    infinite: true,
    dots: false,
    speed: 500,
    responsive: [
      {
        breakpoint: 600,
        settings: {
          arrows: false,
          adaptiveHeight:false,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
  });
  document.addEventListener("click", function(event) {
    let id = event.target.dataset.toggleId;
    if (!id) return;
    let elem = document.getElementById(id);
    if (elem.className == "vspliv d-flex flex-column mol")
    {elem.className = "vspliv d-flex flex-column";}
    else
    {elem.className = "vspliv d-flex flex-column mol";}
  });
